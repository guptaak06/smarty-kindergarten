<?php

add_action( 'wp_enqueue_scripts', 'smarty_child_enqueue_parent_styles', 100 );

function smarty_child_enqueue_parent_styles() {

    wp_enqueue_style( 'stm-style', get_template_directory_uri() . '/style.css', array( 'bootstrap' ), SMARTY_THEME_VERSION, 'all' );
	
	$dir = get_stylesheet_directory() . '/vc_templates/'.smarty_get_layout_mode();
    vc_set_shortcodes_templates_dir( $dir );
}