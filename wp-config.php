<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'author-site');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '.z[6*cdIYyc>-`|2u!k.(MK#;R{[/%ZAXB853cePU)ytZYee^L0>!qBf?+G8OZ?F');
define('SECURE_AUTH_KEY',  '9Wbx_a1)c@4inq61$0A4(X,w?Nyi>B,jTM+K~^)7lISV*!W[#<ZLpmnNJ}f)E[em');
define('LOGGED_IN_KEY',    't%O1HclHfci!{`B4raoAO^TQ1R>G,{.X]EKb/JQ(oBWL=gwx=(}2.8dP@2&OxQ/U');
define('NONCE_KEY',        ':-TXzkb$}W*EasBW<;>8c}Owmzx4E?z9xY2ds<(m,Hf2rPdZ-fT,Pt|v)jgN#1g@');
define('AUTH_SALT',        '<GUw6m^-P}p.v_>Kt8&AlLd$S@SL(C#qJlaY2MFyy9%h=e59M(]tEJ/I+-`S{=@ ');
define('SECURE_AUTH_SALT', '|3G&DkNQBllUwxfVB#$wD;D/lB3;Y8M@w#{(jW$LaEt~?%2<~Y_$Q3T06,T^]^~j');
define('LOGGED_IN_SALT',   'BQJorRN.`wIQ-t- FI]<8e~pr-m7NPKV?DHiUT3~E?G7HcVNfvWka1*A9Ex1j^I>');
define('NONCE_SALT',       'j/;D# TgI]~0I9&9;V`b)@wVJ<Z.Bk2*<5g7vL:%9g6>w=U;f@$&3n`tb)IDaX@_');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

//define memory limit 
define( 'WP_MEMORY_LIMIT', '256M' );
/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
